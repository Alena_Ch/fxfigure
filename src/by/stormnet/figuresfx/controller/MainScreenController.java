package by.stormnet.figuresfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {
    @FXML
    private TextArea logArea;
    @FXML
    private TextArea msgArea;
    @FXML
    private Button btnSend;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnSend.addEventHandler(MouseEvent.MOUSE_CLICKED, evt -> sendButtonClicked());
    }

    private void sendButtonClicked() {
        logArea.appendText(String.format("%s\n",msgArea.getText()));
        msgArea.clear();
    }
}
